
LANG=en_US.UTF-8
LC_CTYPE=en_US.UTF-8
if [ -d "$HOME/.android-studio/bin" ] ; then
  PATH="$PATH:$HOME/.android-studio/bin"
fi
PATH="$PATH:$(ruby -e 'print Gem.user_dir')/bin"
export GEM_HOME=$HOME/.gem
