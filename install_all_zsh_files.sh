#!/bin/bash

zsh="/usr/bin/zsh"
if [ ! -f "$zsh" ]
then
	sudo apt install -y zsh
fi
rm ~/.zgen .zgen-setup .zsh .zsh_aliases .zsh_functions .zsh-quickstart-kit .zshrc .zshrc.d
#cp -R zsh/* ~/
#ln -s "$(pwd)/zsh/.zgen" ~/.zgen
#ln -s "$(pwd)/zsh/.zsh" ~/.zsh
#ln -s "$(pwd)/zsh/.zsh-quickstart-kit-master" ~/.zsh-quickstart-kit
#ln -s "$(pwd)/zsh/.zshrc.d" ~/.zshrc.d

ln -s $(pwd)/zsh/.zsh-quickstart-kit/zsh/.zshrc ~/.zshrc
ln -s $(pwd)/zsh/.zsh-quickstart-kit/zsh/.zsh_functions ~/.zsh_functions
ln -s $(pwd)/zsh/.zsh-quickstart-kit/zsh/.zsh_aliases ~/.zsh_aliases
ln -s $(pwd)/zsh/.zsh-quickstart-kit/zsh/.zgen-setup ~/.zgen-setup

