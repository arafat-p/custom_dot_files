#!/bin/bash
#             _   _     _      _
#  __ _  ___ | |_| |__ | | ___| |_ _   _
# / _` |/ _ \| __| '_ \| |/ _ \ __| | | |
#| (_| | (_) | |_| |_) | |  __/ |_| |_| |
# \__, |\___/ \__|_.__/|_|\___|\__|\__,_|
# |___/
#       https://www.youtube.com/user/gotbletu
#       https://twitter.com/gotbletu
#       https://plus.google.com/+gotbletu
#       https://github.com/gotbletu
#       gotbleu@gmail.com

# Tutorial video: [youtube]https://www.youtube.com/watch?v=4LouA5E76FM[/youtube]
# Custom Actions that can be used on any File Manager with Custom Actions Support
# This script is to resize images to different resolution
# Requirements: imagemagick, zenity
# Works with jpg png, works with tiff but sometimes gives fault messages but still does it.

# thunar custom actions
# command: /path/to/script %N
# note: %N is the selected filenames (without paths)
# conditions: image files

PICKSIZE=$(zenity --list --radiolist --height "255" --name="Image-Resizer" --title="Resize" --text "<b>Please</b> make a selection:" --hide-header --column "Pick" --column "Item" FALSE "96x96" FALSE "128x128" FALSE "640x480" FALSE "800x600" FALSE "1024x768" FALSE "1280x720" FALSE "1280x960" TRUE "1920x1080")
#changed line below so that there is a dir created specifically for the chosen size
#
mkdir -p resized-images/$PICKSIZE
#mkdir -p resized-images
FILES="$@"
myArray=( "$@" )
for arg in "${myArray[@]}"; do
  convert "$arg" -resize "$PICKSIZE" -unsharp 0x0.75+0.75+0.008 "${arg%.*}"_"$PICKSIZE"_RESIZED."${arg##*.}"
  #convert "$arg" -resize "$PICKSIZE" "${arg%.*}"_"$PICKSIZE"_RESIZED."${arg##*.}"
  #find . -name "*_800x600_resized.jpg" -print0 | xargs -0 -I {} mv {} /home/dka/Pictures/converted_images/
  find . -maxdepth 1 -iname "*_resized*" -type f -print0 | xargs -0 -I {} mv {} ~/Pictures/resized-images/$PICKSIZE
done