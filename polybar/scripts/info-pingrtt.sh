#!/bin/sh

#HOST=127.43.12.64
HOST=8.8.8.8

if ! ping=$(ping -n -c 1 -W 1 $HOST); then
    echo "%{F#E70C30} Ping failed %{F-}"
else
    rtt=$(echo "$ping" | sed -rn 's/.*time=([0-9]{1,})\.?[0-9]{0,} ms.*/\1/p')

    if [ "$rtt" -lt 50 ]; then
    icon="%{F#3cb703}%{F-}"
	elif [ "$rtt" -lt 100 ]; then
		icon="%{F#FF007FFF}%{F-}"
	elif [ "$rtt" -lt 150 ]; then
		icon="%{F#FF5500FF}%{F-}"
	elif [ "$rtt" -lt 200 ]; then
		icon="%{F#414DFFFF}%{F-}"
	elif [ "$rtt" -lt 500 ]; then
		icon="%{F#FFFF00FF}%{F-}"
	elif [ "$rtt" -lt 700 ]; then
		icon="%{F#FFAA00FF}%{F-}"
	elif [ "$rtt" -lt 1000 ]; then
		icon="%{F#FF0000FF}%{F-}"
	else
		icon="%{F#d60606}%{F-}"
	fi

    echo " $icon $rtt ms "
fi